export default interface IUser {
  username: string;
  currentRoomId?: number;
}
