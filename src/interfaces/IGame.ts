export default interface IGame {
  timerBeforeGame: number;
  timerDuringGame: number;
  text: string;
}
