import IUser from './IUser';

export default interface IPlayer {
  user: IUser;
  isReady: boolean;
  inputtedText: number;
  progress: number;
}
