import { io, Socket } from 'socket.io-client';
import { DefaultEventsMap } from 'socket.io-client/build/typed-events';

import { SOCKET_URL } from './config';

export const openConnection = (
  username: string | null,
): Socket<DefaultEventsMap, DefaultEventsMap> | null => {
  if (!username) {
    return null;
  }

  return io(SOCKET_URL, { query: { username } });
};
