import React, { useState, useEffect } from 'react';

import { TIMER_DELAY } from '../../../constants';

function Timer({
  time,
  id = '',
  className = '',
  text = '',
  onEnd = () => {},
}: {
  time: number;
  id?: string;
  className?: string;
  text?: string;
  onEnd?: () => void;
}) {
  const [timer, setTimer] = useState<number>(time);

  useEffect(() => {
    setTimer(time);
  }, [time]);

  useEffect(() => {
    if (timer === 0) {
      onEnd();
      return;
    }

    setTimeout(() => setTimer(timer - 1), TIMER_DELAY);
  }, [timer]);

  return (
    <div id={id} className={`${className}`}>
      {`${timer} ${text}`}
    </div>
  );
}

export default Timer;
