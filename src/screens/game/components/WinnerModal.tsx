import React from 'react';

import { IPlayer } from '../../../interfaces';

function WinnerModal({
  results,
  onClose,
}: {
  results: IPlayer[];
  onClose: () => void;
}) {
  return (
    <div className="modal-wrapper">
      <fieldset className="winner-modal modal-wrapper__winner-modal">
        <legend>Game is over</legend>
        <div
          id="quit-results-btn"
          className="winner-modal__close-btn winner-modal-close-btn"
          onClick={onClose}
        >
          x
        </div>
        {results.map((player, index) => (
          <span
            id={`place-${index + 1}`}
            className="winner-modal-place"
            key={index}
          >
            {index + 1}. {player.user.username}
          </span>
        ))}
      </fieldset>
    </div>
  );
}

export default WinnerModal;
