import React from 'react';

function CommentBot({
  comment,
  className = '',
}: {
  comment: string;
  className?: string;
}) {
  if (!comment) {
    return null;
  }

  return (
    <fieldset className={`comment-bot ${className}`}>
      <legend>Comment Bot</legend>
      {comment}
    </fieldset>
  );
}

export default CommentBot;
