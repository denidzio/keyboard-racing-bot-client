import React, { useState, useEffect } from 'react';
import { Socket } from 'socket.io-client';
import { DefaultEventsMap } from 'socket.io-client/build/typed-events';

import { IRoom, IGameResponse, IGame, IPlayer } from '../../../interfaces';
import { UserProgress, KeyboardRacer, WinnerModal, CommentBot } from './';
import { getTextById } from '../../../services/text.service';
import {
  getCurrentPlayerInRoom,
  sortPlayers,
} from '../../../helpers/players.helper';

function Room({
  room,
  socket,
  onExit,
}: {
  room: IRoom | undefined;
  socket: Socket<DefaultEventsMap, DefaultEventsMap>;
  onExit: () => void;
}) {
  const [isReady, setIsReady] = useState<boolean>(false);
  const [game, setGame] = useState<IGame | undefined>();
  const [comment, setComment] = useState<string | undefined>();
  const [results, setResults] = useState<IPlayer[] | undefined>();

  const handleToggleState = () => {
    socket.emit('TOGGLE_PLAYER_STATE');
  };

  const handleInputChar = (char: string) => {
    socket.emit('INPUT_CHAR', char);
  };

  const handleStartGame = async (game: IGameResponse) => {
    const { text } = await getTextById(game.textId);

    const newGame: IGame = {
      text,
      timerBeforeGame: game.timerBeforeGame,
      timerDuringGame: game.timerDuringGame,
    };

    setGame(newGame);
  };

  const handleExit = () => {
    onExit();
    socket.emit('ROOM_EXIT');
  };

  useEffect(() => {
    if (!results || !game) {
      return;
    }

    setGame(undefined);
    setResults(undefined);
  }, [results]);

  useEffect(() => {
    if (!room) {
      return;
    }

    const currentPlayer = getCurrentPlayerInRoom(room);

    if (!currentPlayer) {
      return;
    }

    setIsReady(currentPlayer.isReady);
  }, [room]);

  useEffect(() => {
    if (!socket) {
      return;
    }

    socket.on('SEND_COMMENT', setComment);
    socket.on('START_GAME', handleStartGame);
    socket.on('GAME_IS_OVER', setResults);

    return () => {
      socket.offAny();
    };
  }, [socket]);

  if (!room || !socket) {
    return null;
  }

  return (
    <div className="_container game-room" id="game-page">
      {/* {results ? (
        <WinnerModal results={results} onClose={handleCloseModal} />
      ) : null} */}
      <section className="game-info">
        <header className="game-info-header game-info__header">
          <h1 className="game-info-header__title">{room.name}</h1>
          {!game ? (
            <button
              id="quit-room-btn"
              className="game-info-header-button"
              onClick={handleExit}
            >
              Back To Rooms
            </button>
          ) : null}
        </header>
        <div className="game-info-users game-info__users">
          {sortPlayers(room.players).map((player) => (
            <UserProgress
              className="game-info-users__user"
              player={player}
              key={player.user.username}
            />
          ))}
        </div>
      </section>
      <KeyboardRacer
        className="game-room__keyboard-racer"
        game={game}
        isReady={isReady}
        onToggleState={handleToggleState}
        onInputChar={handleInputChar}
      />
      {comment ? (
        <CommentBot comment={comment} className="game-room__bot" />
      ) : null}
    </div>
  );
}

export default Room;
