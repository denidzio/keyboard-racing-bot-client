import React, { useState, useEffect, Fragment } from 'react';

import { IGame } from '../../../interfaces';
import { Timer } from './';

let text = '';

const isWholeTextEntered = (text: string, part: string) => {
  return text.length === part.length;
};

function KeyboardRacer({
  className = '',
  game,
  isReady,
  onToggleState = () => {},
  onInputChar = () => {},
}: {
  className?: string;
  game: IGame | undefined;
  isReady: boolean;
  onToggleState?: () => void;
  onInputChar?: (char: string) => void;
}) {
  const [timerBeforeIsEnd, setTimerBeforeIsEnd] = useState(false);
  const [inputtedText, setInputtedText] = useState(text);

  const handleKeyUp = (e: globalThis.KeyboardEvent) => {
    if (!game) {
      return;
    }

    const char = e.key;

    if (char !== game.text[text.length]) {
      return;
    }

    onInputChar(char);

    text += char;
    setInputtedText(text);
  };

  useEffect(() => {
    if (timerBeforeIsEnd || !game) {
      window.addEventListener('keyup', handleKeyUp);
    }

    return () => {
      window.removeEventListener('keyup', handleKeyUp);
    };
  });

  useEffect(() => {
    if (!game) {
      return;
    }

    text = '';

    setTimerBeforeIsEnd(false);
    setInputtedText(text);
  }, [game]);

  const renderGame = () => {
    if (!game) {
      return (
        <button
          id="ready-btn"
          className="ready-btn keyboard-racer__btn"
          onClick={() => onToggleState()}
        >
          {isReady ? 'Not Ready' : 'Ready'}
        </button>
      );
    }

    if (timerBeforeIsEnd) {
      return (
        <Fragment>
          {game ? (
            <Timer
              time={game.timerDuringGame}
              className="keyboard-racer-timer keyboard-racer-timer_in-game"
              text="seconds left"
            />
          ) : null}
          <div className="keyboard-racer__text-container">
            <div
              id="text-container"
              className="keyboard-racer-text keyboard-racer-text_primary"
            >
              {game.text}
            </div>
            <div className="keyboard-racer-text">
              <span className="keyboard-racer-text_secondary">
                {inputtedText}
              </span>
              {!isWholeTextEntered(game.text, text) ? (
                <span className="keyboard-racer-text__pointer">_</span>
              ) : null}
            </div>
          </div>
        </Fragment>
      );
    }

    if (!timerBeforeIsEnd) {
      return (
        <Timer
          id="timer"
          time={game.timerBeforeGame}
          onEnd={() => setTimerBeforeIsEnd(true)}
          className="keyboard-racer-timer keyboard-racer-timer_before-game"
        />
      );
    }
  };

  return (
    <fieldset className={`keyboard-racer ${className}`}>
      <legend>Keyboard Racer</legend>
      <div className="keyboard-racer__container">{renderGame()}</div>
    </fieldset>
  );
}

export default KeyboardRacer;
