import {
  setSessionStorageItem,
  getObjectFromSessionStorage,
} from './sessionStorage.helper';

export const setSessionUsername = (username: string | null): void => {
  setSessionStorageItem('username', { username: username });
};

export const getSessionUsername = (): string | null => {
  return getObjectFromSessionStorage('username')?.username;
};
